package com.galvanize;

public class ZipCodeProcessor {

    // don't alter this code...
    private final Verifier verifier;

    public ZipCodeProcessor(Verifier verifier) {

        this.verifier = verifier;
    }

    // write your code below here...
    public String process (String zip){
        String response;

        try{
            verifier.verify(zip);
        }catch(InvalidFormatException e){
            return e.getMessage();
        }catch(NoServiceException e) {
            return e.getMessage();
        }

        return "Thank you!  Your package will arrive soon.";
    }
}
