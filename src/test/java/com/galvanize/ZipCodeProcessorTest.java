package com.galvanize;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ZipCodeProcessorTest {

    // write your tests here
    @Test
    public void assertTrue() {
        assertEquals(true, true);
    }

    @Test
    public void processShouldAcceptValidZipAndReturnThankYou() {
        Verifier verifier = new Verifier();
        ZipCodeProcessor test = new ZipCodeProcessor(verifier);
        String response = test.process("85387");
        assertEquals("Thank you!  Your package will arrive soon.", response);
    }

    @Test
    public void processShouldRejectTooShortZip() {
        Verifier verifier = new Verifier();
        ZipCodeProcessor test = new ZipCodeProcessor(verifier);
        String response = test.process("8538");
        assertEquals("The zip code you entered was the wrong length.", response);
    }

    @Test
    public void processShouldRejectTooLongZip() {
        Verifier verifier = new Verifier();
        ZipCodeProcessor test = new ZipCodeProcessor(verifier);
        String response = test.process("853871");
        assertEquals("The zip code you entered was the wrong length.", response);
    }

    @Test
    public void processShouldRejectZipThatStartsWithOne() {
        Verifier verifier = new Verifier();
        ZipCodeProcessor test = new ZipCodeProcessor(verifier);
        String response = test.process("15387");
        assertEquals("We're sorry, but the zip code you entered is out of our range.", response);
    }
}